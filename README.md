# Usage

## Gradle

```bash
cat input.txt | gradle run
echo -e '55 22 33 44 55\n11 22 33 11 22\n33 33 33 33 33\n44 44 44 44 44\n55 55 55 55 55' | gradle run
echo -e 'foo\nbar' | gradle run
```

## Jar

Create uberjar

```bash
gradle shadowJar
```

Run from command line


```bash
cat input.txt | java -jar build/libs/treasurehunt-1.0-SNAPSHOT-all.jar
echo -e 'foo\nbar' | java -jar build/libs/treasurehunt-1.0-SNAPSHOT-all.jar
```
