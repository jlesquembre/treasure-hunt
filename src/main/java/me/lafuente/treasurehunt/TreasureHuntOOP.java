package me.lafuente.treasurehunt;

import java.util.HashSet;

public class TreasureHuntOOP extends TreasureHuntFunctional {

    class Coordinate{
        private final int row;
        private final int col;
        private final int[][] table;

        public Coordinate(int row, int col, int[][] table){
            this.row = row;
            this.col = col;
            this.table = table;
        }

        public String asString(){
            return String.format("%d %d\n", row, col);
        }

        public boolean hasTreasure(){
            return table[row-1][col-1] == Integer.valueOf(String.format("%d%d", row, col));
        }


        public Coordinate next(){
            int[] value =  TreasureHuntFunctional.splitDigits(table[row-1][col-1]);
            return new Coordinate(value[0], value[1], this.table);
        }

        public boolean equals(Object o){
            if(!(o instanceof Coordinate))
                return false;
            System.out.println(this.asString());
            return (((Coordinate) o).col == col) && (((Coordinate) o).row == row);
        }

        public int hashCode(){
            StringBuffer buffer = new StringBuffer();
            buffer.append(this.row);
            buffer.append(this.col);
            return buffer.toString().hashCode();
        }

    }

    public String find(){
        if(this.table == null){
            return notFound;
        }
        return findOOP(new Coordinate(1, 1, this.table));
    }

    private String findOOP(Coordinate coordinate){
        String result = "";
        HashSet<Coordinate> visited = new HashSet<>();
        Coordinate coord = coordinate;
        while(true){
            visited.add(coord);
            result += coord.asString();
            if(coord.hasTreasure()){
                break;
            }
            coord = coord.next();
            if(visited.contains(coord)){
                result = notFound;
                break;
            }
        }
        return result;
    }
}
