package me.lafuente.treasurehunt;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.stream.Stream;

public class TreasureHuntFunctional {
    protected final static String notFound = "NO TREASURE";
    protected int[][] table;
    private final int rows;
    private final int cols;
    // Bottom right corner
    private final int corner;


    public TreasureHuntFunctional() {
        this(5,5);
    }


    public TreasureHuntFunctional(int rows, int cols) {
        this.rows = rows;
        this.cols = cols;
        this.corner = Integer.valueOf(String.format("%s%s", rows, cols));
        this.table = new int[rows][cols];
        try{
            this.table = this.readStdin();
        } catch(IllegalArgumentException err){
            this.table = null;
        }
    }


    protected Integer valueOf(String string){
        try{
            return Integer.valueOf(string);
        } catch (NumberFormatException err){
            throw new IllegalArgumentException("Invalid number: " + string);
        }
    }


    protected int[] toInt(String row){
        int[] result = Stream.of(row.split(" "))
            .mapToInt(this::valueOf)
            .filter(x -> x >= 11 && x <= this.corner )
            .toArray();

        if (result.length != this.cols) throw new IllegalArgumentException("Not enough values");
        return result;

    }


    public int[][] readStdin() throws IllegalArgumentException {
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        int[][] coordinates = in.lines()
                .limit(this.cols)
                .map(this::toInt)
                .toArray(int[][]::new);

        if (coordinates.length != this.rows) throw new IllegalArgumentException("Not enough values");
        return coordinates;
    }


    public String find(){
        if(this.table == null){
            return notFound;
        }
        return findRec(11, "", new HashSet<>());
    }


    public void findStdout(){
        System.out.println(this.find());
    }

    static int[] splitDigits(int number){
        return Integer.toString(number)
                .codePoints()
                .mapToObj(c -> String.valueOf((char) c))
                .mapToInt(Integer::valueOf)
                .toArray();

    }


    private String findRec(int coordinate, String path, HashSet<Integer> visited){

        // Avoid recursive paths
        if(visited.contains(coordinate)) return notFound;

        int [] digits = splitDigits(coordinate);

        int row = digits[0], col = digits[1];
        int cellValue = this.table[row-1][col-1];

        String newPath = String.format("%s%d %d\n",path, row, col);
        if(cellValue == coordinate) return newPath;

        visited.add(coordinate);
        return findRec(cellValue, newPath, visited);
    }

    public static void main(String[] args){
        TreasureHuntFunctional hunt = new TreasureHuntFunctional();
        hunt.findStdout();
    }
}
