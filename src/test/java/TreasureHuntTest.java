import me.lafuente.treasurehunt.TreasureHuntFunctional;
import me.lafuente.treasurehunt.TreasureHuntOOP;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;

import static org.junit.Assert.assertEquals;

public class TreasureHuntTest {

    private final InputStream systemIn = System.in;
    private final PrintStream systemOut = System.out;

    private ByteArrayInputStream testIn;
    private ByteArrayOutputStream testOut;

    @Before
    public void setUpOutput() {
        testOut = new ByteArrayOutputStream();
        System.setOut(new PrintStream(testOut));
    }

    private void provideInput(String data) {
        testIn = new ByteArrayInputStream(data.getBytes());
        System.setIn(testIn);
    }

    private String getOutput() {
        return testOut.toString();
    }

    @After
    public void restoreSystemInputOutput() {
        System.setIn(systemIn);
        System.setOut(systemOut);
    }

    @Test
    public void testBadInput(){
        String testString = "aa\n22\n";
        provideInput(testString);
        new TreasureHuntFunctional().findStdout();
        assertEquals("NO TREASURE\n", getOutput());
    }

    @Test
    public void testBadInput2(){
        String testString = "1\n22\n";
        provideInput(testString);
        new TreasureHuntFunctional().findStdout();
        assertEquals("NO TREASURE\n", getOutput());
    }

    @Test
    public void testNoRows(){
        String testString = "11 11 11 11 11\n22 22 22 22 22";
        provideInput(testString);
        new TreasureHuntFunctional().findStdout();
        assertEquals("NO TREASURE\n", getOutput());
    }

    @Test
    public void testNoCols(){
        String testString = "11\n22\n";
        provideInput(testString);
        new TreasureHuntFunctional().findStdout();
        assertEquals("NO TREASURE\n", getOutput());
    }

    @Test
    public void testFound() {

        String testString =
                "55 14 25 52 21\n" +
                "44 31 11 53 43\n" +
                "24 13 45 12 34\n" +
                "42 22 43 32 41\n" +
                "51 23 33 54 15";
        provideInput(testString);
        assertEquals(
                new TreasureHuntFunctional().find(),
                "1 1\n" +
                "5 5\n" +
                "1 5\n" +
                "2 1\n" +
                "4 4\n" +
                "3 2\n" +
                "1 3\n" +
                "2 5\n" +
                "4 3\n"
        );
    }

    @Test
    public void testRecursion(){
        String testString =
                "55 14 25 52 21\n" +
                "44 31 11 53 43\n" +
                "24 13 45 12 34\n" +
                "42 22 43 32 41\n" +
                "51 23 33 54 11";
        provideInput(testString);
        assertEquals(new TreasureHuntFunctional().find(), "NO TREASURE" );
    }

    @Test
    public void testNoPath(){
        String testString =
                "55 14 25 52 21\n" +
                "44 31 11 53 43\n" +
                "24 13 45 12 34\n" +
                "42 22 45 32 41\n" +
                "51 23 33 54 15";
        provideInput(testString);
        assertEquals(new TreasureHuntFunctional().find(), "NO TREASURE" );
    }

    @Test
    public void testFoundOOP() {

        String testString =
                "55 14 25 52 21\n" +
                "44 31 11 53 43\n" +
                "24 13 45 12 34\n" +
                "42 22 43 32 41\n" +
                "51 23 33 54 15";
        provideInput(testString);
        assertEquals(
                new TreasureHuntOOP().find(),
                "1 1\n" +
                "5 5\n" +
                "1 5\n" +
                "2 1\n" +
                "4 4\n" +
                "3 2\n" +
                "1 3\n" +
                "2 5\n" +
                "4 3\n"
        );
    }

    @Test
    public void testRecursionOOP(){
        String testString =
                "55 14 25 52 21\n" +
                "44 31 11 53 43\n" +
                "24 13 45 12 34\n" +
                "42 22 43 32 41\n" +
                "51 23 33 54 11";
        provideInput(testString);
        assertEquals(new TreasureHuntOOP().find(), "NO TREASURE" );
    }

    @Test
    public void testNoPathOOP(){
        String testString =
                "55 14 25 52 21\n" +
                "44 31 11 53 43\n" +
                "24 13 45 12 34\n" +
                "42 22 45 32 41\n" +
                "51 23 33 54 15";
        provideInput(testString);
        assertEquals(new TreasureHuntOOP().find(), "NO TREASURE" );
    }
}
